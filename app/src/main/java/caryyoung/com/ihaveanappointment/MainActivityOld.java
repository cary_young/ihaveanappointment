//package caryyoung.com.ihaveanappointment;
//
//import android.support.v4.view.ViewCompat;
//import android.os.Bundle;
//import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
//import android.support.v7.app.AppCompatActivity;
//import android.view.View;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import org.joda.time.DateTime;
//import org.joda.time.DateTimeZone;
//import org.joda.time.Instant;
//
//public class MainActivity extends AppCompatActivity {
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//
//        final long appointment = getSharedPreferences("i_have_an_appointment_prefs", MODE_PRIVATE).getLong("appointment", -1L);
//
//        System.out.println("Appt found: " + appointment);
//        if (appointment != -1) {
//            showCheckInView();
//        } else {
//            showHelloView();
//        }
//    }
//
//    private void showCheckInView() {
//        final long appointment = getSharedPreferences("i_have_an_appointment_prefs", MODE_PRIVATE).getLong("appointment", -1L);
//        final TextView checkInView = (TextView) findViewById(R.id.activity_main_check_in);
//        checkInView.setAlpha(1f);
//        checkInView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Instant now = Instant.now();
//
//                // if they kept their appointment, grats.
//                boolean appointmentWasKept = Math.abs(appointment - now.getMillis()) <= 1000 * 60 * 3;
//                appointmentWasKept = true;
//                if (appointmentWasKept) {
//                    ViewCompat.animate(checkInView).alpha(0f).setDuration(300).setListener(new ViewPropertyAnimatorListenerAdapter() {
//                        @Override
//                        public void onAnimationEnd(View view) {
//                            showCongratulationsView();
//                            System.out.println("YOU KEPT YOUR APPOINTMENT");
//                        }
//                    }).start();
//
//                    // update streak count
//                } else {
////                    showGameOver();
//                    System.out.println("BE MORE PUNCTUAL");
//                }
//            }
//        });
//    }
//
//    private void showCongratulationsView() {
//        final TextView congratsView = (TextView) findViewById(R.id.activity_main_thank_you);
//        ViewCompat.animate(congratsView).alpha(1f).setDuration(300).start();
//        ViewCompat.animate(congratsView).setDuration(2000).setListener(new ViewPropertyAnimatorListenerAdapter() {
//            @Override
//            public void onAnimationEnd(View view) {
//                ViewCompat.animate(congratsView).alpha(0f).setDuration(300).setListener(new ViewPropertyAnimatorListenerAdapter() {
//                    @Override
//                    public void onAnimationEnd(View view) {
//                        updateAppointment();
//                    }
//                }).start();
//            }
//        }).start();
//    }
//
//    private void showHelloView() {
//        final TextView helloView = (TextView) findViewById(R.id.activity_main_request);
//        final View apptView = findViewById(R.id.activity_main_appointment_screen);
//        helloView.setAlpha(1f);
//        helloView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                ViewCompat.animate(helloView).alpha(0f).setDuration(1000).setListener(
//                        new ViewPropertyAnimatorListenerAdapter() {
//
//                            @Override
//                            public void onAnimationEnd(View view) {
//                                updateAppointment();
//                                transitionBetweenViews(helloView,apptView, 0, 300);
//                            }
//                        }
//                ).start();
//            }
//        });
//    }
//
//    private void updateAppointment() {
//        final LinearLayout appointmentLayout = (LinearLayout) findViewById(R.id.activity_main_appointment_screen);
//        final View checkinView = findViewById(R.id.activity_main_check_in);
//        // get a new appointment date
//        Instant newAppointmentInstant = AppointmentGenerator.createAppointment(true, true, 0);
//        DateTime newAppointment = new DateTime(newAppointmentInstant, DateTimeZone.getDefault());
//
//        ((TextView) findViewById(R.id.activity_main_appointment_time)).setText(newAppointment.toString("h:mm a"));
//        ((TextView) findViewById(R.id.activity_main_appointment_date)).setText(newAppointment.toString("MMMM d, y"));
//
//        // save it to prefs
//        getSharedPreferences("i_have_an_appointment_prefs", MODE_PRIVATE).edit().putLong("appointment", newAppointmentInstant.getMillis()).apply();
//    }
//
//    private void transitionBetweenViews(View view1, final View view2, long fadeout, final long fadein) {
//        ViewCompat.animate(view1).alpha(0f).setDuration(fadeout).setListener(new ViewPropertyAnimatorListenerAdapter() {
//            @Override
//            public void onAnimationEnd(View view) {
//                ViewCompat.animate(view2).alpha(1f).setDuration(fadein).start();
//            }
//        }).start();
//    }
//}
