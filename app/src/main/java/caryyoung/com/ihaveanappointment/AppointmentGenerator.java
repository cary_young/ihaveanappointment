package caryyoung.com.ihaveanappointment;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Duration;
import org.joda.time.Instant;

import java.util.Random;

/**
 * Created by cary on 3/15/16.
 */
public class AppointmentGenerator {
    private static final Random random = new Random();

    public static Instant createAppointment(boolean day, boolean night, int streak) {
        Instant appointment = Instant.now();
        System.out.println("Current time: " + appointment);
        switch (streak) {
            case 0:
                appointment = appointment.plus(Duration.standardMinutes(random.nextInt(4)));
                System.out.println("Generated appointment: " + appointment);
                break;
            case 1:
                appointment = appointment.plus(Duration.standardMinutes(random.nextInt(10)));
                System.out.println("Generated appointment: " + appointment);
                break;
            case 2:
                appointment = appointment.plus(Duration.standardMinutes(random.nextInt(25)));
                System.out.println("Generated appointment: " + appointment);
                break;
            case 3:
                appointment = appointment.plus(Duration.standardMinutes(1));
                appointment = appointment.plus(Duration.standardMinutes(random.nextInt(59)));
                System.out.println("Generated appointment: " + appointment);
                break;
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                appointment = appointment.plus(Duration.standardMinutes(random.nextInt(59))
                        .plus(Duration.standardHours(random.nextInt(24))));
                break;
            default:
                appointment = appointment.plus(Duration.standardMinutes(random.nextInt(59))
                        .plus(Duration.standardHours(random.nextInt(24))))
                        .plus(Duration.standardDays(7));
                break;
        }
        if (streak <= 3 || validate(day, night, appointment)) {
            return appointment;
        } else {
            return createAppointment(day, night, streak);
        }
    }

    private static boolean validate(boolean day, boolean night, Instant apptInstant) {
        DateTime dt = new DateTime(apptInstant, DateTimeZone.getDefault());
        boolean appointmentIsDaytime = dt.getHourOfDay() > 9 && dt.getHourOfDay() < 19;
        if (!day && appointmentIsDaytime) {
            return false;
        } else if (!night && !appointmentIsDaytime) {
            return false;
        }
        return true;
    }
}
