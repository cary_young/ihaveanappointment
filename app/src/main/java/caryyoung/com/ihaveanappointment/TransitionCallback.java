package caryyoung.com.ihaveanappointment;

/**
 * Created by cary on 3/16/16.
 */
public interface TransitionCallback {
    void onTransitionComplete();
}
