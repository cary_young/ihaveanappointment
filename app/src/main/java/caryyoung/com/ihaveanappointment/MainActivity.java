package caryyoung.com.ihaveanappointment;

import android.support.v4.view.ViewCompat;
import android.os.Bundle;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Instant;

public class MainActivity extends AppCompatActivity {

    private View checkInView;
    private View requestView;
    private View congratsView;
    private View appointmentView;
    private View sorryView;
    private TextView streakView;

    private View current;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void init() {
        setContentView(R.layout.activity_main);

        setupViews();

        final long appointment = getSharedPreferences("i_have_an_appointment_prefs", MODE_PRIVATE).getLong("appointment", -1L);

        System.out.println("Appt found: " + appointment);
        if (appointment != -1) {
            showCheckInView();
        } else {
            showHelloView();
        }
    }

    private void setupViews() {
        checkInView = findViewById(R.id.activity_main_check_in);
        requestView = findViewById(R.id.activity_main_request);
        appointmentView = findViewById(R.id.activity_main_appointment_screen);
        congratsView = findViewById(R.id.activity_main_thank_you);
        sorryView = findViewById(R.id.activity_main_sorry_screen);
        streakView = (TextView) findViewById(R.id.activity_main_streak_count);
        int streak = getSharedPreferences("i_have_an_appointment_prefs", MODE_PRIVATE).getInt("streak", 0);
        if (streak > 0) {
            streakView.setText(Integer.toString(streak));
        }

        checkInView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (current == checkInView) {
                    System.out.println("CLICKED CHECKIN");
                    Instant now = Instant.now();
                    final long appointment = getSharedPreferences("i_have_an_appointment_prefs", MODE_PRIVATE).getLong("appointment", -1L);
                    int streak = getSharedPreferences("i_have_an_appointment_prefs", MODE_PRIVATE).getInt("streak", -1);
                    System.out.println("Streakk: " + streak);

                    boolean appointmentWasKept = Math.abs(appointment - now.getMillis()) <= 1000 * 60;
                    if (appointmentWasKept) {
                        getSharedPreferences("i_have_an_appointment_prefs", MODE_PRIVATE).edit().putInt("streak", streak + 1).apply();
                        streak = getSharedPreferences("i_have_an_appointment_prefs", MODE_PRIVATE).getInt("streak", -1);
                        streakView.setText(Integer.toString(streak));
                        System.out.println("Streakkkk: " + streak);
                        transition(checkInView, congratsView, 600, 1200, new TransitionCallback() {
                            @Override
                            public void onTransitionComplete() {
                                updateAppointment();
                                transition(congratsView, appointmentView, 1800, 850, new TransitionCallback() {
                                    @Override
                                    public void onTransitionComplete() {
                                        transition(appointmentView, checkInView, 13000, 500, null);
                                    }
                                });
                            }
                        });
                    } else {
                        clearAppointment();
                        clearStreak();
                        transition(checkInView, sorryView, 600, 950, new TransitionCallback() {
                            @Override
                            public void onTransitionComplete() {
                                try {
                                    synchronized (this) {
                                        wait(2800);
                                    }
                                } catch (InterruptedException ex) {
                                }
                                transition(sorryView, requestView, 600, 500, null);
                            }
                        });
                    }
                } else if (current == requestView) {
                    System.out.println("CLICKED REQUEST");
                    updateAppointment();
                    transition(requestView, appointmentView, 1200, 850, new TransitionCallback() {
                        @Override
                        public void onTransitionComplete() {
                            transition(appointmentView, checkInView, 13000, 300, null);
                        }
                    });
                }
            }
        });
    }

    private void showCheckInView() {
        checkInView.setAlpha(1f);
        current = checkInView;
    }

    private void showHelloView() {
        requestView.setAlpha(1f);
        current = requestView;
    }

    private void transition(View toHide, final View toShow, long fadeOut, final long fadeIn, final TransitionCallback onCompletion) {
        ViewCompat.animate(toHide).alpha(0f).setDuration(fadeOut).setListener(new ViewPropertyAnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(View view) {
                current = toShow;
                ViewCompat.animate(toShow).alpha(1f).setDuration(fadeIn).setListener(new ViewPropertyAnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(View view) {
                        if (onCompletion != null) {
                            onCompletion.onTransitionComplete();
                        }
                    }
                }).start();
            }
        }).start();
    }

    private void clearAppointment() {
        getSharedPreferences("i_have_an_appointment_prefs", MODE_PRIVATE).edit().putLong("appointment", -1).apply();
    }

    private void clearStreak() {
        System.out.println("clearing streak");
        getSharedPreferences("i_have_an_appointment_prefs", MODE_PRIVATE).edit().putInt("streak", 0).apply();
        streakView.setText(" ");
    }

    private void updateAppointment() {
        int streak = getSharedPreferences("i_have_an_appointment_prefs", MODE_PRIVATE).getInt("streak", 0);
        System.out.println("STREAK: " + streak);

        // get a new appointment date
        Instant newAppointmentInstant = AppointmentGenerator.createAppointment(true, true, streak);
        DateTime newAppointment = new DateTime(newAppointmentInstant, DateTimeZone.getDefault());

        ((TextView) findViewById(R.id.activity_main_appointment_time)).setText(newAppointment.toString("h:mm a"));
        ((TextView) findViewById(R.id.activity_main_appointment_date)).setText(newAppointment.toString("MMMM d, y"));

        // save it to prefs
        getSharedPreferences("i_have_an_appointment_prefs", MODE_PRIVATE).edit().putLong("appointment", newAppointmentInstant.getMillis()).apply();
    }

}
